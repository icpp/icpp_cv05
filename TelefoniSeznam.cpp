//
// Created by david on 11.11.2019.
//

#include "TelefoniSeznam.h"

using namespace Model;

TelefoniSeznam::TelefoniSeznam(){
    this ->_start = nullptr;
}

TelefoniSeznam::~TelefoniSeznam(){
    PrvekSeznam* item = _start;
    while (item != nullptr) {
        PrvekSeznam* temporary = item->_next;
        delete item;
        item = temporary;
    }

    _start = nullptr;
}

void TelefoniSeznam::pridejOsobu(Entity::Osoba aOsoba){
    PrvekSeznam* item = new PrvekSeznam();
    item->_data = aOsoba;
    item->_next = _start;
    _start = item;
}


//Vyhledavani dle jmena
std::string TelefoniSeznam::najdiTelefon(std::string aJmeno) const {
    if (aJmeno.size() == 0) {
        throw std::invalid_argument("Nebyl zadano zadne jmeno.");
    }

    PrvekSeznam* item = _start;

    while (item != nullptr) {
        if (item->_data.getJmeno() == aJmeno) {
            return item->_data.getTelefon();
        }
        item = item->_next;
    }
    throw std::invalid_argument("Zadany uzivatel nebyl nalezen.");
}

//Vyhledavani dle ID
std::string TelefoniSeznam::najdiTelefon(int aId) const {
    if (aId < 0) {
        throw std::invalid_argument("Nebyl zadano zadne id.");
    }

    PrvekSeznam* item = _start;

    while (item != nullptr) {
        if (item->_data.getId() == aId) {
            return item->_data.getTelefon();
        }
        item = item->_next;
    }
    throw std::invalid_argument("Zadany uzivatel nebyl nalezen.");
}

Model::TelefoniSeznam::PrvekSeznam::PrvekSeznam()
{
}

Model::TelefoniSeznam::PrvekSeznam::~PrvekSeznam()
{
}









