//
// Created by david on 11.11.2019.
//

#ifndef ICPP_CV05_TELEFONISEZNAM_H
#define ICPP_CV05_TELEFONISEZNAM_H

#include <string>
#include "Osoba.h"

namespace Model {
    class TelefoniSeznam
    {
    public:
        TelefoniSeznam();
        ~TelefoniSeznam();
        void pridejOsobu(Entity::Osoba aOsoba);
        std::string najdiTelefon(std::string aJmeno) const;
        std::string najdiTelefon(int aId)const;

        class PrvekSeznam
        {
        public:
            PrvekSeznam();
            ~PrvekSeznam();
            Entity::Osoba _data;
            PrvekSeznam* _next;
        };

    private:
        PrvekSeznam* _start;
    };
}




#endif //ICPP_CV05_TELEFONISEZNAM_H
