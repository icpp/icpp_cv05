//
// Created by david on 11.11.2019.
//

#include "Osoba.h"

namespace Entity {
    Osoba::Osoba(std::string aJmeno, std::string aTelefon, int aId) {
         Osoba::_jmeno = aJmeno;
         Osoba::_telefon = aTelefon;
         Osoba::_id = aId;
    }

    std::string Osoba::getJmeno() const {
        return _jmeno;
    }

    int Osoba::getId() const {
        return _id;
    }

    std::string  Osoba::getTelefon() const {
        return _telefon;
    }

    Entity::Osoba::~Osoba()
    {
    }


}


