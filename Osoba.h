#pragma once
#include <iostream>

namespace Entity
{
    class Osoba
    {
    public:
        Osoba() {};
        Osoba(std::string aJmeno, std::string aTelefon, int aId);
        ~Osoba();
        std::string getJmeno()const;
        std::string getTelefon()const;
        int getId()const;
    private:
        int _id;
        std::string _jmeno;
        std::string _telefon;
    };
}
